# Easily publish an R bookdown website with GitLab Pages

This repository is a bookdown demo website served on `GitLab Pages` using `GitLab CI`.

See https://rtthomas.gitlab.io/bookdown-gitlab-pages 

